#include <iostream>
#include <iomanip>
#include <queue>
#include <tuple>
#include <fstream>
#include <algorithm>
#include <numeric>
#include <sstream>

#include "lodepng/lodepng.h"

template<class... Args>
void write(Args&&... args) {
    ((std::cout << std::setprecision(10)) << ... << args) << "\n";
}

constexpr std::uint32_t digit_mask = 0b11111100;
constexpr char border_mask = 0b001;
constexpr char done_mask = 0b010;
constexpr char uninteresting_mask = done_mask | border_mask;
constexpr std::size_t irrelevant_particle_size = 5;

void ensure(bool cond, std::string message) {
    if (!cond) {
        write("Error: ", message);
        std::exit(1);
    }
}

auto load_image(std::string filename)
{
    std::vector<unsigned char> result;
    unsigned w;
    unsigned h;
    auto err = lodepng::decode(result, w, h, filename);
    ensure(!err, std::string{"[lodepng] - "} + lodepng_error_text(err));
    write("Loaded png of size ", h, "x", w);
    return std::make_tuple(result, h, w);
}

auto convert_to_greyscale(std::vector<unsigned char> data) {
    std::vector<char> new_image(data.size() / 4);
    uint32_t* data_view = reinterpret_cast<std::uint32_t*>(data.data());

    for (std::size_t i = 0; i < new_image.size(); ++i) {

        constexpr std::uint32_t mask = (digit_mask << 16) | (digit_mask << 8) | (digit_mask);
        new_image[i] = static_cast<bool>((data_view[i] & mask) ^ mask);
    }

    return new_image;
}

void add_neighbours(
    const std::pair<unsigned, unsigned>& curr,
    unsigned h, unsigned w,
    std::vector<char>& data,
    std::queue<std::pair<unsigned, unsigned>>& queue
) {
    const auto check = [&](unsigned new_h, unsigned new_w) {
        if (new_h < h && new_w < w) { // unsigned, so 1 chekc is ok
            std::size_t ind = new_h * w + new_w;
            if (! (data[ind] & uninteresting_mask)) {
                queue.emplace(new_h, new_w);
                data[ind] |= done_mask;
            }
        }
    };

    check(curr.first, curr.second + 1);
    check(curr.first, curr.second - 1);
    check(curr.first + 1, curr.second);
    check(curr.first - 1, curr.second);
}

auto check_interesting_area(std::vector<char>& data, std::size_t index, unsigned h, unsigned w) {
    unsigned curr_h = index / w;
    unsigned curr_w = index % w;
    std::size_t area_size = 0;

    std::queue<std::pair<unsigned, unsigned>> to_check;
    to_check.emplace(curr_h, curr_w);

    while(!to_check.empty()) {
        auto curr = to_check.front();
        to_check.pop();
        add_neighbours(curr, h, w, data, to_check);
        ++area_size;
    }

    return area_size;
}

auto calculate_areas(std::vector<char>& data, unsigned h, unsigned w, float pixel_area)
{
    const std::size_t size = h * w;
    std::vector<float> area_sizes;

    std::size_t i = 0;
    while (true) {
        while (i < size && (data[i] & uninteresting_mask)) {
            ++i;
        }
        if (i < size) {
            auto curr_area_size = check_interesting_area(data, i, h, w);
            if (curr_area_size > irrelevant_particle_size) {
                area_sizes.push_back(curr_area_size * pixel_area);
            }
            ++i;
        } else {
            break;
        }
    }
    return area_sizes;
}

void save_check_image(const std::vector<char>& v, unsigned h, unsigned w, std::string name) {
    name = name.substr(0, name.size() - 4) + "_out.png";
    std::vector<unsigned char> temp(v.size() * 4, 0);
    for (std::size_t i = 0; i < v.size(); ++i) {
        temp[i * 4] = bool(v[i] & border_mask) * 255;
        temp[i * 4 + 1] = bool(v[i] & done_mask) * 255;
        temp[i * 4 + 3] = 255;
    }

    auto err = lodepng::encode(name, temp, w, h);
    write(name);
    ensure(!err, std::string{"[lodepng] - "} + lodepng_error_text(err));
    write("Saved verification image as ", name,
        "\n\t(red: uninteresting area/border, green: processed area)");
}

float variance(const std::vector<float> &vec, float mean)
{
    size_t size = vec.size();
    if (size == 1) {
        return 0.f;
    } else {
        return std::accumulate(vec.begin(), vec.end(), 0.f,
            [&](float accumulator, float val)
            {
                return accumulator + ((val - mean) * (val - mean)) / (size - 1);
            }
       );
    }
}

float median(const std::vector<float>& areas) {
    return areas.size() % 2
        ? areas[areas.size() / 2]
        : (areas[areas.size() / 2 - 1] + areas[areas.size() / 2]) / 2.f;
}

void calculate_statistics(const std::vector<float>& areas) {
    write("All numers are meant in microns");
    write("Min: ", areas.back());
    write("Max: ", areas.front());
    write("Count: ", areas.size());

    auto mean = std::accumulate(areas.begin(), areas.end(), 0.f) / areas.size();
    write("Mean: ", mean);
    write("Median: ", median(areas));
    write("Variance: ", variance(areas, mean));
}

void save_area_sizes(const std::vector<float>& v, std::string name) {
    name = name.substr(0, name.size() - 4) + "_out.txt";
    std::ofstream file{name};
    for (const auto area_size : v) {
        file << area_size << "\n";
    }
    write("Saved area sizes as ", name);
}

template<class... Args>
constexpr auto lines(Args&&... args) {
    std::stringstream ss;
    const auto f = [&](auto&& a) { ss << a << "\n"; return true; };
    bool a[] = { ((ss << args << "\n"), true)... };
    return ss.str();
}

int main(int argc, char* argv[]) {

    const auto help = lines(
        "\tFilename: name of the source png file (e.g alma.png)",
        "\tScale: number of pixels/micron (e.g 0.82)",
        "\tExample: '<Name of the program> alma.png 1.234'",
        "",
        "The program considers a pixel a particle area, if it's RGB values are all bigger than " + std::to_string(digit_mask) + " (if it's white).",
        "",
        "The program considers areas smaller than " + std::to_string(irrelevant_particle_size) + " pixels irrelevant since "
        "they are most likely artifacts in the picture file.",
    "");
    if (argc > 1 && (argv[1] == std::string("-h")
        || argv[1] == std::string("-help")
        || argv[1] == std::string("--h")
        || argv[1] == std::string("--help")
        || argv[1] == std::string("help"))
    ) {
        write("Usage:\n", help);
    } else {
        ensure(argc > 2, "Filename and Scale arguments needed\n" + help);
        std::string filename = argv[1];
        auto [data, h, w] = load_image(filename);
        auto pixel_area = std::stof(argv[2]);
        auto work_data = convert_to_greyscale(data);

        auto areas = calculate_areas(work_data, h, w, pixel_area);
        std::sort(areas.begin(), areas.end(), std::greater{});

        write("\n **** Data ****");
        calculate_statistics(areas);
        write(" **************\n");
        save_check_image(work_data, h, w, filename);

        write();
        save_area_sizes(areas, filename);
    }
}
